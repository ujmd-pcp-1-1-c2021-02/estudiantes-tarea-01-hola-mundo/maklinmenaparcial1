﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace parcial1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void LLENAR_Click(object sender, EventArgs e)
        {
            tmrRellenar.Enabled = true;
            tmrVaciar.Enabled = false;
        }

        private void VACIAR_Click(object sender, EventArgs e)
        {
            tmrVaciar.Enabled = true;
            tmrRellenar.Enabled = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void tmrRellenar_Tick(object sender, EventArgs e)
        {
            if (AGUADELTANQUE.Height <= 194)
            {
                AGUADELTANQUE.Height = AGUADELTANQUE.Height + 12;
                AGUADELTANQUE.Top = AGUADELTANQUE.Top - 12;
            }
            else
            {
                tmrRellenar.Enabled = false;
            }
        }

        private void tmrVaciar_Tick(object sender, EventArgs e)
        {
            if (AGUADELTANQUE.Height >= 12)
            {
                AGUADELTANQUE.Height = AGUADELTANQUE.Height - 12;
                AGUADELTANQUE.Top = AGUADELTANQUE.Top + 12;
            }
            else
            {
                tmrVaciar.Enabled = false;
            }
        }
    }
}
