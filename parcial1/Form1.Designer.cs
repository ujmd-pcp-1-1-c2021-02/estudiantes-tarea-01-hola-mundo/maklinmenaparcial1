﻿
namespace parcial1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TANQUE = new System.Windows.Forms.Panel();
            this.INTERIORDELTANQUE = new System.Windows.Forms.Panel();
            this.AGUADELTANQUE = new System.Windows.Forms.Panel();
            this.PANELDEBOTONES = new System.Windows.Forms.Panel();
            this.LLENAR = new System.Windows.Forms.Button();
            this.VACIAR = new System.Windows.Forms.Button();
            this.tmrRellenar = new System.Windows.Forms.Timer(this.components);
            this.tmrVaciar = new System.Windows.Forms.Timer(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.TANQUE.SuspendLayout();
            this.INTERIORDELTANQUE.SuspendLayout();
            this.PANELDEBOTONES.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.SuspendLayout();
            // 
            // TANQUE
            // 
            this.TANQUE.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.TANQUE.Controls.Add(this.INTERIORDELTANQUE);
            this.TANQUE.Location = new System.Drawing.Point(260, 12);
            this.TANQUE.Name = "TANQUE";
            this.TANQUE.Size = new System.Drawing.Size(149, 212);
            this.TANQUE.TabIndex = 0;
            // 
            // INTERIORDELTANQUE
            // 
            this.INTERIORDELTANQUE.BackColor = System.Drawing.SystemColors.Window;
            this.INTERIORDELTANQUE.Controls.Add(this.AGUADELTANQUE);
            this.INTERIORDELTANQUE.Location = new System.Drawing.Point(3, 3);
            this.INTERIORDELTANQUE.Name = "INTERIORDELTANQUE";
            this.INTERIORDELTANQUE.Size = new System.Drawing.Size(143, 206);
            this.INTERIORDELTANQUE.TabIndex = 1;
            // 
            // AGUADELTANQUE
            // 
            this.AGUADELTANQUE.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.AGUADELTANQUE.Location = new System.Drawing.Point(0, 159);
            this.AGUADELTANQUE.Name = "AGUADELTANQUE";
            this.AGUADELTANQUE.Size = new System.Drawing.Size(143, 47);
            this.AGUADELTANQUE.TabIndex = 0;
            // 
            // PANELDEBOTONES
            // 
            this.PANELDEBOTONES.Controls.Add(this.VACIAR);
            this.PANELDEBOTONES.Location = new System.Drawing.Point(705, 283);
            this.PANELDEBOTONES.Name = "PANELDEBOTONES";
            this.PANELDEBOTONES.Size = new System.Drawing.Size(83, 29);
            this.PANELDEBOTONES.TabIndex = 1;
            // 
            // LLENAR
            // 
            this.LLENAR.BackColor = System.Drawing.SystemColors.WindowText;
            this.LLENAR.ForeColor = System.Drawing.SystemColors.Window;
            this.LLENAR.Location = new System.Drawing.Point(3, 3);
            this.LLENAR.Name = "LLENAR";
            this.LLENAR.Size = new System.Drawing.Size(75, 23);
            this.LLENAR.TabIndex = 0;
            this.LLENAR.Text = "LLENAR";
            this.LLENAR.UseVisualStyleBackColor = false;
            this.LLENAR.Click += new System.EventHandler(this.LLENAR_Click);
            // 
            // VACIAR
            // 
            this.VACIAR.BackColor = System.Drawing.SystemColors.WindowText;
            this.VACIAR.ForeColor = System.Drawing.SystemColors.Window;
            this.VACIAR.Location = new System.Drawing.Point(3, 3);
            this.VACIAR.Name = "VACIAR";
            this.VACIAR.Size = new System.Drawing.Size(75, 23);
            this.VACIAR.TabIndex = 1;
            this.VACIAR.Text = "VACIAR";
            this.VACIAR.UseVisualStyleBackColor = false;
            this.VACIAR.Click += new System.EventHandler(this.VACIAR_Click);
            // 
            // tmrRellenar
            // 
            this.tmrRellenar.Tick += new System.EventHandler(this.tmrRellenar_Tick);
            // 
            // tmrVaciar
            // 
            this.tmrVaciar.Tick += new System.EventHandler(this.tmrVaciar_Tick);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::parcial1.Properties.Resources.soporte1;
            this.pictureBox2.Location = new System.Drawing.Point(500, 312);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(65, 106);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::parcial1.Properties.Resources.base4;
            this.pictureBox1.Location = new System.Drawing.Point(260, 218);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(149, 174);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::parcial1.Properties.Resources.soporte1;
            this.pictureBox4.Location = new System.Drawing.Point(140, 234);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 158);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 5;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox5.Location = new System.Drawing.Point(403, 218);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(55, 10);
            this.pictureBox5.TabIndex = 6;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox6.Location = new System.Drawing.Point(455, 218);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(10, 72);
            this.pictureBox6.TabIndex = 7;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox7.Location = new System.Drawing.Point(455, 283);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(30, 10);
            this.pictureBox7.TabIndex = 8;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox8.Location = new System.Drawing.Point(482, 262);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(100, 50);
            this.pictureBox8.TabIndex = 9;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox9.Location = new System.Drawing.Point(570, 302);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(21, 10);
            this.pictureBox9.TabIndex = 10;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox10.Location = new System.Drawing.Point(588, 302);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(11, 48);
            this.pictureBox10.TabIndex = 11;
            this.pictureBox10.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 548);
            this.splitter1.TabIndex = 12;
            this.splitter1.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox11.Location = new System.Drawing.Point(239, 218);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(27, 10);
            this.pictureBox11.TabIndex = 13;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox12.Location = new System.Drawing.Point(140, 192);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(100, 50);
            this.pictureBox12.TabIndex = 14;
            this.pictureBox12.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LLENAR);
            this.panel1.Location = new System.Drawing.Point(705, 234);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(83, 29);
            this.panel1.TabIndex = 2;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox3.Location = new System.Drawing.Point(184, 144);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(10, 52);
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox13.Location = new System.Drawing.Point(94, 144);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(100, 10);
            this.pictureBox13.TabIndex = 16;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox14.Location = new System.Drawing.Point(94, 104);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(10, 50);
            this.pictureBox14.TabIndex = 17;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox15.Location = new System.Drawing.Point(0, 75);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(51, 50);
            this.pictureBox15.TabIndex = 18;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox16.Location = new System.Drawing.Point(48, 104);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(56, 10);
            this.pictureBox16.TabIndex = 19;
            this.pictureBox16.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(920, 548);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PANELDEBOTONES);
            this.Controls.Add(this.TANQUE);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.TANQUE.ResumeLayout(false);
            this.INTERIORDELTANQUE.ResumeLayout(false);
            this.PANELDEBOTONES.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel TANQUE;
        private System.Windows.Forms.Panel INTERIORDELTANQUE;
        private System.Windows.Forms.Panel AGUADELTANQUE;
        private System.Windows.Forms.Panel PANELDEBOTONES;
        private System.Windows.Forms.Button VACIAR;
        private System.Windows.Forms.Button LLENAR;
        private System.Windows.Forms.Timer tmrRellenar;
        private System.Windows.Forms.Timer tmrVaciar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
    }
}

